Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations' }
  resources :users do
  	get 'requests', to: 'connection#see_friend_request', as: 'friend_request'
  	post 'accept_request', to: 'connection#accept_request', as: 'accept_request'
  	post 'reject_request', to: 'connection#reject_request', as: 'reject_request'
  end
  root to: 'users#index'
  post 'send_request', to: 'connection#get_friend_request', as: 'new_friend_request'

  resources :posts
  # resources :connections, only: [:new, :create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
