class CreateConnections < ActiveRecord::Migration[5.2]
  def change
    create_table :connections do |t|
      t.references :user, foreign_key: true
      t.references :request, foreign_key: false
      t.boolean :status, default: false

      t.timestamps
    end
    add_foreign_key :connections, :users, column: :request_id
  end
end
