class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    user = User.find(current_user.id)
    @friends = user.friends
    @pending_invites = user.pending_sent_requests
    @requests = user.requests
    @rest_all_users = user.all_except
    @rest_all_users = @rest_all_users - @requests - @friends - @pending_invites
  end

  def show
  end
end
