class PostsController < ApplicationController

  before_action :authenticate_user!

  def index
    @posts = []
    current_user.friends.each do |friend|
      @posts = @posts + friend.posts
    end
    @posts = @posts + current_user.posts
    @posts = @posts.shuffle
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.new(post_params)
    if @post.save
      notice = 'Posted!'
    else
      notice = 'Not able to create post, please try again!'
    end
    redirect_to(posts_path, notice: notice)
  end

  def show
    @post = Post.find(params[:id])
  end

  def destroy
    @post = Post.find(params[:id])
    if @post.destroy
      notice = 'Post deleted!'
    else
      notice = 'Not able to delete post, please try again!'
    end
    redirect_to(posts_path, notice: notice)
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if @post.update(post_params)
      notice = 'Post updated!'
    else
      notice = 'Not able to update post, please try again!'
    end
    redirect_to(post_path(@post), notice: notice)
  end

  private

  def post_params
    params.require(:post).permit(:title, :body)
  end
end
