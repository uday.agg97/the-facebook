class ConnectionController < ApplicationController

  before_action :authenticate_user!

  def get_friend_request
    connection = Connection.new(user_id: current_user.id, request_id: params[:id])
    if connection.save
      notice = 'Request sent!'
    else
      notice = 'Request failed, please try again'
    end
    redirect_to(users_path, notice: notice)
  end

  def see_friend_request
    @requests = current_user.requests
  end

  def accept_request
    connection = Connection.find_request(params[:id], current_user.id).first
    if connection.update(status: true) && Connection.create(user_id: current_user.id, request_id: params[:id], status: true)
      notice = 'Request accepted!'
    else
      notice = 'Failed to accept request, please try again'
    end
    redirect_to(users_path, notice: notice)
  end

  def reject_request
    connection = Connection.find_request(params[:id], current_user.id).first
    if connection.destroy
      notice = 'Request rejected!'
    else
      notice = 'Failed to reject request, please try again'
    end
    redirect_to(users_path, notice: notice)
  end
end
