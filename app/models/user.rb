class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :connections, -> { where status: true }
  has_many :friends, through: :connections, source: :request
  has_many :receiving_requests, -> { where status: false }, class_name: 'Connection', foreign_key: 'request_id'
  has_many :requests, through: :receiving_requests, source: :user
  has_many :sent_requests, -> { where status: false }, class_name: 'Connection'
  has_many :pending_sent_requests, through: :sent_requests, source: :request

  has_many :posts


  def all_except
    User.where.not(id: self.id)
  end
end
