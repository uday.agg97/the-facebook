class Connection < ApplicationRecord
  belongs_to :user
  belongs_to :request, class_name: 'User'

  def self.find_request(id, request_id)
    self.where("user_id = ? and request_id = ?", id, request_id)
  end
end
